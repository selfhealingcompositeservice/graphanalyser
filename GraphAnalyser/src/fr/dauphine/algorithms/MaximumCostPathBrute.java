package fr.dauphine.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.service.Service;

public class MaximumCostPathBrute {
	
	Graph<Service, Number> graph;
	int maximumCostPathIndex;
	
	public MaximumCostPathBrute(Graph<Service, Number> graph) {
		this.graph = graph;
	}
	
	Map<String,Double> maximumCostPathFromNode = new HashMap<String, Double>();
			
	public double getMaximumCostPath(Service i, Service f) {
		
	
		//Initialize
		for(Service v:graph.getVertices()) {
			maximumCostPathFromNode.put(v.getName(), new Double(0));
		}

		//get all possible paths in the graph
		AllPaths(graph, i, f);
		
		int pathCost;
		double maxCostPath = 0.0;
		int index = 0;
		//calculate the cost of each path
		for(List<Service> p:allPaths) {
			
			Map<String,Double> costFromNode = new HashMap<String, Double>();
			
			pathCost = 0;
			for(Service v:p) {
				pathCost += v.getEstimatedExecutionTime();
				
				for(String k:costFromNode.keySet())
					costFromNode.put(k, costFromNode.get(k) + v.getEstimatedExecutionTime());
				
				costFromNode.put(v.getName(), v.getEstimatedExecutionTime());
			}
			
			if(pathCost > maxCostPath) {
				maxCostPath = pathCost;
				maximumCostPathIndex = index;
			}
			
			for(String k:costFromNode.keySet())
				if(costFromNode.get(k) > this.maximumCostPathFromNode.get(k))
					this.maximumCostPathFromNode.put(k, costFromNode.get(k));
			
			index++;
		}
		
		return maxCostPath;
	}
	
	public List<String> getResultPath() {
		List<String> resultPath = new ArrayList<String>();
		
		for(Service s:allPaths.get(maximumCostPathIndex)) 
			resultPath.add(s.getName());
		
		return resultPath;
	}
	

	
	private  static Stack<Service> path  = new Stack<Service>();   // the current path
    private  static List<Service> onPath  = new ArrayList<Service>();     // the set of vertices on the path
    
    private  static List<List<Service>> allPaths = new ArrayList<List<Service>>();

    public static void AllPaths(Graph<Service, Number> graph, Service i, Service f) {
        enumerate(graph, i , f);
    }

    // use DFS
    private static void enumerate(Graph<Service, Number> graph,  Service v, Service t) {

        // add node v to current path from s
        path.push(v);
        onPath.add(v);

        // found path from s to t - currently prints in reverse order because of stack
        if (v.getName().equals(t.getName())) 
        	allPaths.add(new ArrayList< Service>(path));

        // consider all neighbors that would continue path with repeating a node
        else {
            for ( Service w : graph.getSuccessors(v)) {
                if (!onPath.contains(w)) enumerate(graph, w, t);
            }
        }

        // done exploring from v, so remove from path
        path.pop();
        onPath.remove(v);
    }

}
