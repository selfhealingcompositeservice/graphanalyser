package fr.dauphine.algorithms;

import java.util.*;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;
import fr.dauphine.service.Service;

public class BellmanFord  {
    LinkedList<Edge> edges;
    List<String> vertices;
    Map<String, Double> d;
    Map<String, String> p;
    int n,e;
    String source;
    final double INFINITY =Double.POSITIVE_INFINITY;

    private static class Edge  {
        String u,v;
        double w;

        public Edge(String u, String v, double w)     {
            this.u=u;
            this.v=v;
            this.w=w;
        }
    }

    public BellmanFord (Graph<Service, Number> graph) {
        
    	
        edges = new LinkedList<Edge>();
        vertices = new ArrayList<String>();

      
        n = graph.getVertexCount();

        for(Number edge:graph.getEdges()) {
        	Pair<Service> endpoints = graph.getEndpoints(edge);
        	edges.add(new Edge(endpoints.getFirst().getName(),
        			endpoints.getSecond().getName(),
        			-endpoints.getFirst().getEstimatedExecutionTime()));
        }
        

        e = edges.size();
        d = new HashMap<String, Double>();
        p = new HashMap<String, String>();
        
        for(Service s:graph.getVertices()) {
        	vertices.add(s.getName());
        	d.put(s.getName(), INFINITY);
        	p.put(s.getName(), null);
        }

    }

    public void relax(String source) {
    	this.source = source;

        for(String s:vertices) {
        	d.put(s, INFINITY);
        	p.put(s, null);
        }

        d.put(source, 0.0);
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < e; ++j) {
                if (d.get(edges.get(j).u) + edges.get(j).w < d.get(edges.get(j).v)) {
                    d.put(edges.get(j).v, d.get(edges.get(j).u) + edges.get(j).w);
                    p.put(edges.get(j).v,  edges.get(j).u);
                }
             }
         }
    }
    
    public double getResult(String vertex) {
    	return -d.get(vertex);
    }
    
    public List<String> getResultPath(String vertex) {
    	List<String> resultPath = new ArrayList<String>();
    	
    	resultPath.add(vertex);
    	String pred = p.get(vertex);
    	while(pred != null) {
    		resultPath.add(pred);
        	pred = p.get(pred);
        }
        Collections.reverse(resultPath);
        
        return resultPath;
    }

}