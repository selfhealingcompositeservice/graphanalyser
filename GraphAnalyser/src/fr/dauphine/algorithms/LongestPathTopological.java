package fr.dauphine.algorithms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Service;

public class LongestPathTopological {
	
	final static double INFINITY =Double.NEGATIVE_INFINITY;
	
	public static <E> Map<String, Double> getLongestPath(Graph<Service, Number> graph, String initialNode, List<Service> topologicalSort) {
		
		Map<String, Double> dist = new HashMap<String, Double>();
		
	    // Initialize distances to all vertices as infinite and distance
	    // to source as 0
	    for (Service s:graph.getVertices())
	        dist.put(s.getName(), INFINITY);
	    
	    dist.put(initialNode, 0.0);
	    
	    for(Service s:topologicalSort) {
	    	// Update distances of all adjacent vertices
	        //list<AdjListNode>::iterator i;
	        if (dist.get(s.getName()) != INFINITY) {
	        	for(Service i:graph.getSuccessors(s)) {
		         // for (i = adj[s].begin(); i != adj[s].end(); ++i)
		             if (dist.get(i.getName()) < dist.get(s.getName()) + GraphUtils.getVertexByName(graph, i.getName()).getEstimatedExecutionTime())
		                dist.put(i.getName(), dist.get(s.getName()) + GraphUtils.getVertexByName(graph, i.getName()).getEstimatedExecutionTime());
	        	}
	        }
	    }
	    return dist;
	}

}
