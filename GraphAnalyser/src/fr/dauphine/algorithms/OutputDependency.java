package fr.dauphine.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class OutputDependency {
	
	private Map<String, Set<Data>> dependencies;
	private Map<String, Set<String>> allPredecessors;
	private Graph<Service, Number> graph;
	private Service controlFinal;
	
	public OutputDependency(Graph<Service, Number> graph) {
		this.graph = graph;
		this.controlFinal = GraphUtils.getVertexByName(graph, Constants.FINAL_NODE);
	}
	
	private void setVertexImportance() {
		
		int totalOutputs = controlFinal.getInputs().size();
		dependencies = new HashMap<String, Set<Data>>();
		for(Service s:graph.getPredecessors(GraphUtils.getVertexByName(graph, Constants.FINAL_NODE)))
			if(!s.isControl()) {
				//we put immediately 
				dependencies.put(s.getName(), new HashSet<Data>(GraphUtils.listIntersection(s.getOutputs(),controlFinal.getInputs())));

				traverseVertices(s, new HashSet<Data>(GraphUtils.listIntersection(s.getOutputs(),controlFinal.getInputs())));
			}
		
		for(Service s:graph.getVertices())
			if(!s.isControl())
				s.setOutputDependency((double)dependencies.get(s.getName()).size()/totalOutputs);
	}
	
	private void traverseVertices(Service service, Set<Data> outputs) {
		
		if(dependencies.get(service.getName()) != null)
			dependencies.get(service.getName()).addAll(outputs);
		else
			dependencies.put(service.getName(), new HashSet<Data>(outputs));
		
		dependencies.get(service.getName()).addAll(GraphUtils.listIntersection(service.getOutputs(), controlFinal.getInputs()));
		
		for(Service s:graph.getPredecessors(service)) 
			traverseVertices(s, dependencies.get(service.getName()));
	}

	public Map<String, Set<Data>> getDependencies() {
		setVertexImportance();
		return dependencies;
	}

	public Map<String, Set<String>> getAllPredecessors() {
		
		allPredecessors = new HashMap<>();
		
		for(Service s:graph.getSuccessors(GraphUtils.getVertexByName(graph, Constants.INITIAL_NODE)))
			if(!s.isControl()) {
				
				Set<String> pred = new HashSet<>();
				traverseVerticesSucc(s, pred);
			}

		return allPredecessors;
	}

	private void traverseVerticesSucc(Service s, Set<String> pred) {
		if(allPredecessors.get(s.getName()) != null)
			allPredecessors.get(s.getName()).addAll(pred);
		else
			allPredecessors.put(s.getName(), pred);
		
		for(Service succ:graph.getSuccessors(s))
			if(!s.isControl()) {
				Set<String> pred2 = new HashSet<>(pred);
				pred2.add(s.getName());
				traverseVerticesSucc(succ, pred2);
			}
	}
	
}