package fr.dauphine.graphanalyser;

public class TimeContext {
	
	/**
	 * Estimated Time of service execution
	 */
	private double estimatedExecutionTime;

	/**
	 * Estimated Time just before the firing of a service
	 */
	private double expectedTime;
	
	/**
	 * Estimated remaining time from the end of the service until the
	 * end of the composite service
	 */
	private double remainingTime;
	
	/**
	 * Additional execution time the service can use without
	 * exceeding the estimated execution time of the composition
	 */
	private double freeTime;
	
	/**
	 * Estimated execution time of the composition
	 */
	private double globalEstimatedTime;

	public double getExpectedTime() {
		return expectedTime;
	}

	public void setExpectedTime(double expectedTime) {
		this.expectedTime = expectedTime;
	}

	public double getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(double remainingTime) {
		this.remainingTime = remainingTime;
	}

	public double getFreeTime() {
		return freeTime;
	}

	public void setFreeTime(double freeTime) {
		this.freeTime = freeTime;
	}

	public double getGlobalEstimatedTime() {
		return globalEstimatedTime;
	}

	public void setGlobalEstimatedTime(double globalEstimatedTime) {
		this.globalEstimatedTime = globalEstimatedTime;
	}

	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}

	public void setEstimatedExecutionTime(double estimatedExecutionTime) {
		this.estimatedExecutionTime = estimatedExecutionTime;
	}
	
}
