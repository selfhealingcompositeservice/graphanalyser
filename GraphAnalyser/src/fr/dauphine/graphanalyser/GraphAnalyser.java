package fr.dauphine.graphanalyser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.BellmanFord;
import fr.dauphine.algorithms.LongestPathTopological;
import fr.dauphine.algorithms.OutputDependency;
import fr.dauphine.algorithms.TopologicalSort;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.service.CompositeTransactionalProperty;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import fr.dauphine.service.TransactionalProperty;

public class GraphAnalyser {
	
	Graph<Service, Number> graph;
	
	public GraphAnalyser(String graph, String graphQoS) {
		this(GraphReader.getGraph(graph, graphQoS));
	}
	
	public GraphAnalyser(Graph<Service, Number> graph) {
		this.graph = graph;
	}

	public double getEstimatedExecutionTime(String initialNode, String finalNode) {
		BellmanFord  r = new BellmanFord (graph);
        r.relax(initialNode);
		return r.getResult(finalNode);
	}
	
	public double getEstimatedExecutionTime() {
		/*boolean clean = false;
		if(GraphUtils.getVertexByName(graph, Constants.INITIAL_NODE) == null) {
			clean = true;
			GraphUtils.addControlNodes(graph);
		}
		
		BellmanFord  r = new BellmanFord (graph);
        r.relax(Constants.INITIAL_NODE);
        
        if(clean)
        	GraphUtils.removeControlNodes(graph);
        
		return r.getResult(Constants.FINAL_NODE);*/
		List<Service> topologicalResult = TopologicalSort.sort(graph);
		
		Map<String, Double> topResult = LongestPathTopological.getLongestPath(graph, fr.dauphine.service.Constants.INITIAL_NODE, topologicalResult);
		double globalEstimatedTime = topResult.get(fr.dauphine.service.Constants.FINAL_NODE);
		return globalEstimatedTime;
	}
	
	public Map<String, Set<Data>> getOutputDependency() {
		OutputDependency o = new OutputDependency(graph);
		return o.getDependencies();
	}
	
	public Map<String, Set<String>> getAllPredecessors() {
		OutputDependency o = new OutputDependency(graph);
		return o.getAllPredecessors();
	}

	public double getEstimatedPrice() {
		double price = 0.0;
		
		for(Service s:graph.getVertices())
			price += s.getPrice();
		
		return price;
	}

	public double getEstimatedReputation() {
		double reputation = -1;
	
		
		return reputation;
	}

	public double getEstimatedAvailability() {
		double availability = 1;
		
		for(Service s:graph.getVertices())
			availability *= s.getAvailability();
		
		return availability;
	}
	
	public CompositeTransactionalProperty getTransactionalProperty() {
		
		boolean retriable = true, compensable = true;
		
		
		for(Service s:graph.getVertices())
			if(s.getTransactionalProperty().equals(TransactionalProperty.Pivot))
				return CompositeTransactionalProperty.Atomic;
			else if(!retriable && !compensable)
				return CompositeTransactionalProperty.Atomic;
			else if(s.getTransactionalProperty().equals(TransactionalProperty.PivotRetriable))
				compensable = false;
			else if(s.getTransactionalProperty().equals(TransactionalProperty.Compensable))
				retriable = false;
				
		if(compensable && retriable)
			return CompositeTransactionalProperty.CompensableRetriable;
		if(compensable && !retriable)
			return CompositeTransactionalProperty.Compensable;
		if(!compensable && retriable)
			return CompositeTransactionalProperty.AtomicRetriable;
		
		return CompositeTransactionalProperty.None;
	}
	
	public Map<String, TimeContext> getLocalTimeContexts() {
		Map<String, TimeContext> localTimeContexts = new HashMap<String, TimeContext>();
		
		List<Service> topologicalResult = TopologicalSort.sort(graph);
		
		Map<String, Double> topResult = LongestPathTopological.getLongestPath(graph, fr.dauphine.service.Constants.INITIAL_NODE, topologicalResult);
		double globalEstimatedTime = topResult.get(fr.dauphine.service.Constants.FINAL_NODE);
		
		for(String s:topResult.keySet()) {
			TimeContext timeContext = new TimeContext();
			
			timeContext.setGlobalEstimatedTime(globalEstimatedTime);
			
			timeContext.setEstimatedExecutionTime(GraphUtils.getVertexByName(graph, s).getEstimatedExecutionTime());
			
			timeContext.setExpectedTime(topResult.get(s)  - GraphUtils.getVertexByName(graph, s).getEstimatedExecutionTime());
			
			Map<String, Double> r = LongestPathTopological.getLongestPath(graph, s, topologicalResult);
			
			timeContext.setRemainingTime(r.get(fr.dauphine.service.Constants.FINAL_NODE));
			
			timeContext.setFreeTime(timeContext.getGlobalEstimatedTime() - timeContext.getExpectedTime() 
					- timeContext.getEstimatedExecutionTime() 
					- timeContext.getRemainingTime());
			
			localTimeContexts.put(s, timeContext);
		}
		
		return localTimeContexts;
	}
	
}