package fr.dauphine.test;

import java.io.IOException;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.MaximumCostPathBrute;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.service.Service;

public class MaximumCostPathBruteTest {
	
private static Service getServiceByName(String name, Graph<Service, Number> graph) {
		
		for(Service s:graph.getVertices())
			if(s.getName().equals(name))
				return s;
		
		
		return null;
	}
	
	public static void main(String args[]) throws IOException   {
		
    	Graph<Service, Number> graph = GraphReader.getGraph(args[0], args[1]);
        
    	MaximumCostPathBrute mcp = new MaximumCostPathBrute(graph);
       
        System.out.println("Final: " + mcp.getMaximumCostPath(getServiceByName("0",graph), getServiceByName("5",graph)));
        System.out.println("Path: " + mcp.getResultPath());
        
    }

}