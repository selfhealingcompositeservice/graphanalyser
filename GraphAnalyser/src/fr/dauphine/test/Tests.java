package fr.dauphine.test;

import java.util.List;
import java.util.Map;

import com.google.common.base.Stopwatch;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.BellmanFord;
import fr.dauphine.algorithms.LongestPathTopological;
import fr.dauphine.algorithms.TopologicalSort;
import fr.dauphine.generator.GraphGenerator;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.random.RandomQoSGenerator;
import fr.dauphine.service.Service;

public class Tests {
	
	public static void main(String...args) throws Exception {
		new Tests().run();
	}

	private void run() throws Exception {
		GraphGenerator<Service, Number> graphGenerator = new GraphGenerator<>();
		Graph<Service, Number> graph = 
				GraphUtils.toServiceGraph(graphGenerator.generateRandomBarabasiAlbert(5));
		
		RandomQoSGenerator<Number> rQoS = new RandomQoSGenerator<>();
		rQoS.generateQoS(graph);
		
		GraphUtils.addControlNodes(graph);
		
		
		
		for(Service s:graph.getVertices())
			System.out.println(s.getName() + " " +s.getEstimatedExecutionTime() + " " + " p: " + graph.getPredecessors(s) + " s: " + graph.getSuccessors(s));
		
		
		
		List<Service> topologicalResult = TopologicalSort.sort(graph);
		
		
		
		System.out.println("topological sort: " + topologicalResult);
		
		
		
		BellmanFord bf = new BellmanFord(graph);
		bf.relax(fr.dauphine.service.Constants.INITIAL_NODE);
		double bfResult = bf.getResult(fr.dauphine.service.Constants.FINAL_NODE);
		
		System.out.println("bellman-ford result: " + bfResult);
		
		Map<String, Double> topResult = LongestPathTopological.getLongestPath(graph, fr.dauphine.service.Constants.INITIAL_NODE, topologicalResult);
		
		System.out.println("longest path topological result: " + topResult);
		
		if(topResult.get(fr.dauphine.service.Constants.FINAL_NODE) != bfResult)
			throw new Exception("Error calculating longest path");
		
		/*
		 * Performance tests 
		 */
		Stopwatch stopwatchTests = Stopwatch.createStarted();
		for(int i=5; i<100; i++) {
			graph = GraphUtils.toServiceGraph(graphGenerator.generateRandomBarabasiAlbert(i));
			rQoS.generateQoS(graph);
			
			GraphUtils.addControlNodes(graph);
			
			/*
			 * Bellman-Ford
			 */
			Stopwatch stopwatch = Stopwatch.createStarted();
			for(Service s:graph.getVertices()) {
				bf = new BellmanFord(graph);
				bf.relax(s.getName());
				bfResult = bf.getResult(fr.dauphine.service.Constants.FINAL_NODE);
			
			}

			stopwatch.stop();
			long elapsedBF = stopwatch.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
			System.out.println(elapsedBF);
			
			
			/*
			 * Topological Sorting
			 */
			Stopwatch stopwatchTOP = Stopwatch.createStarted();
			for(Service s:graph.getVertices()) {
				topResult = LongestPathTopological.getLongestPath(graph, s.getName(), TopologicalSort.sort(graph));
			}
			stopwatchTOP.stop();
			long elapsedTop = stopwatchTOP.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS);
			
			System.out.println("result: " + i + " BF=" + bfResult + " TOP=" + topResult.get(fr.dauphine.service.Constants.FINAL_NODE));
			System.out.println("time: " + i + " BF=" + elapsedBF + " TOP=" + elapsedTop);
			
			/*
			 * Test result
			 */
			//if(topResult.get(fr.dauphine.service.Constants.FINAL_NODE) != bfResult)
				//throw new Exception("Error calculating longest path in graph " + i);
			
			
		}
		stopwatchTests.stop();
		System.out.println("Finished tests in " + stopwatchTests.elapsed(java.util.concurrent.TimeUnit.MILLISECONDS) + " ms");
	}

}
