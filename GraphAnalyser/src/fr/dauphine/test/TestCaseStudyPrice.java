package fr.dauphine.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.BellmanFord;
import fr.dauphine.algorithms.LongestPathTopological;
import fr.dauphine.algorithms.TopologicalSort;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.graphutils.JSONUtils;
import fr.dauphine.service.Service;

public class TestCaseStudyPrice {

	public static void main(String[] args) {
		Graph<Service, Number> graph = null;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("/home/rafa/Applications/glassfish4/glassfish/domains/domain1/config/casestudy/ehealthapp.graph"));
			graph = JSONUtils.jsonToGraph((JSONObject)obj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		GraphUtils.addControlNodes(graph);
		
		GraphAnalyser analyser = new GraphAnalyser(graph);
	
		double globalPrice = analyser.getEstimatedPrice();
		
		String data = "";
		
		for(Service s:graph.getVertices()) {
			
			data += "name=" + s.getName();
			double price = GraphUtils.getVertexByName(graph, s.getName()).getPrice();
			
			data += ", global_expected_price=" + globalPrice;

			data += ", price=" + price;
			
			data += "\n";
			
			
		}
		
		File file = new File("ehealth.price");
		
		try {
			FileUtils.writeStringToFile(file, data, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
