package fr.dauphine.test;

import java.io.IOException;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.BellmanFord;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.service.Service;

public class BellmanFordTest {
	
	public static void main(String args[]) throws IOException   {
		
    	Graph<Service, Number> graph = GraphReader.getGraph(args[0], args[1]);
        BellmanFord  r = new BellmanFord (graph);
        
        
        r.relax("0");
       
        System.out.println("Final: " + r.getResult("5"));
        System.out.println(r.getResultPath("5"));
        
    }

}