package fr.dauphine.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.algorithms.BellmanFord;
import fr.dauphine.algorithms.LongestPathTopological;
import fr.dauphine.algorithms.TopologicalSort;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphanalyser.TimeContext;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.graphutils.JSONUtils;
import fr.dauphine.service.Service;

public class TestCaseStudyTime {

	public static void main(String[] args) {
		Graph<Service, Number> graph = null;
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("/home/rafa/Applications/glassfish4/glassfish/domains/domain1/config/casestudy/ehealthapp.graph"));
			graph = JSONUtils.jsonToGraph((JSONObject)obj);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		GraphUtils.addControlNodes(graph);
		
		List<Service> topologicalResult = TopologicalSort.sort(graph);
		
		
		
		
		
		
		
		BellmanFord bf = new BellmanFord(graph);
		bf.relax(fr.dauphine.service.Constants.INITIAL_NODE);
		double bfResult = bf.getResult(fr.dauphine.service.Constants.FINAL_NODE);
		
		System.out.println("bellman-ford result: " + bfResult);
		GraphAnalyser analyser = new GraphAnalyser(graph);
		System.out.println("global_expected_time=" + analyser.getEstimatedExecutionTime());

		Map<String, TimeContext> timeContexts = analyser.getLocalTimeContexts();

		String data = "";
		
		for(String s:timeContexts.keySet()) {
			TimeContext timeContext = timeContexts.get(s);
			
			data += "name=" + s;
			
			data += ", global_expected_time=" + timeContext.getGlobalEstimatedTime();
			data += ", eet=" + timeContext.getEstimatedExecutionTime();
			
			data += ", time_before_execution=" + timeContext.getExpectedTime();
			
			Map<String, Double> r = LongestPathTopological.getLongestPath(graph, s, topologicalResult);
			
			data += ", remaining_time=" + timeContext.getRemainingTime();
			data += ", free_time=" + timeContext.getFreeTime();
			
			data += "\n";
			
			
		}

		
		File file = new File("ehealth.time");
		
		try {
			FileUtils.writeStringToFile(file, data, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
